set autoindent
set autoread
set autowrite
set backspace=indent,eol,start
set backup
set backupdir=~/.vim/tmp/backup//
set completeopt=menu
set directory=~/.vim/tmp/swap//
set display=lastline,uhex
set encoding=utf-8
set expandtab
set fileencoding=utf-8
set formatoptions=cql
set hidden
set hlsearch
set ignorecase
set incsearch
set laststatus=2
set listchars=eol:$,tab:>-,space:.
set makeprg=make\ -j
set mouse=a
set nocompatible
set nowrap
set number
set relativenumber
set shiftwidth=4
set showbreak=>>
set smartindent
set smarttab
set softtabstop=4
set statusline=file:%f\|ft:%Y\|ff:%{&ff}\|fenc:%{strlen(&fenc)?&fenc:'none'}\|buf:%n\|%=%%:%p\|row:%l\|col:%c
set tabstop=4
set tags=./tags,tags;/
set undodir=~/.vim/tmp/undo//
set wildmenu
syntax on

" netrw
let g:netrw_altv = 1
let g:netrw_browse_split = 4
let g:netrw_liststyle = 3
let g:netrw_winsize = 26

" debugging
packadd termdebug
let termdebugger = "gdb-multiarch"
"let g:termdebug_wide=1

" clang-format
map <c-k> :py3file /usr/share/clang/clang-format-9/clang-format.py
imap <c-k> <c-o>:py3file /usr/share/clang/clang-format-9/clang-format.py

" vim-plug
call plug#begin('~/.vim/plugged')
"Plug 'autozimu/LanguageClient-neovim', {'branch': 'next', 'do': 'bash ./install.sh'}
Plug 'dhruvasagar/vim-table-mode'
Plug 'majutsushi/tagbar'
Plug 'scrooloose/nerdtree'
Plug 'shougo/vimproc.vim', {'do' : 'make'}
Plug 'talek/obvious-resize'
Plug 'tpope/vim-surround'
Plug 'vim-airline/vim-airline'
Plug 'vim-scripts/DrawIt'
Plug 'vim-voom/VOoM'
Plug 'w0rp/ale'
Plug 'ctrlpvim/ctrlp.vim'
call plug#end()

" LanguageClient Options
"let g:LanguageClient_serverCommands = {'c': ['clangd'], 'cpp': ['clangd'], 'python': ['pyls']}

" Table Mode Options
let g:table_mode_align_char= ':'
let g:table_mode_corner= '+'
let g:table_mode_corner_corner= '+'
let g:table_mode_delimiter = ';'
let g:table_mode_header_fillchar= '='
"let g:table_mode_fillchar= '-'

" NERDTree Options
"autocmd VimEnter * NERDTree
let g:NERDTreeWinSize = 40
let g:NERDTreeWinPos = 'right'

" Obvious Resize Options
noremap <silent> <C-Up> :<C-U>ObviousResizeUp<CR>
noremap <silent> <C-Down> :<C-U>ObviousResizeDown<CR>
noremap <silent> <C-Left> :<C-U>ObviousResizeLeft<CR>
noremap <silent> <C-Right> :<C-U>ObviousResizeRight<CR>
let g:obvious_resize_default = 2

" VOoM Options
let g:voom_default_mode = 'pandoc'
let g:voom_tree_placement = 'right'
let g:voom_tree_width = 50

" ALE Options
let g:ale_c_build_dir_names = ['.', 'build', 'bin']
let g:ale_echo_msg_format = '[%linter%] %s [%severity%]'
let g:ale_lint_on_enter = 1
let g:ale_lint_on_filetype_changed = 1
let g:ale_lint_on_insert_leave = 1
let g:ale_lint_on_save = 1
let g:ale_lint_on_text_changed = 0
let g:ale_linters = {'c': 'all', 'python': ['mypy', 'pylint']}
let g:ale_parse_compile_commands = 1
